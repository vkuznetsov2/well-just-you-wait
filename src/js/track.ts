import Player from "./players/player";
import Obstacle from "./obstacle";
import {cellLength, cellNumber} from "./common";

class Track {
    private readonly player: Player;
    private readonly obstacle: Obstacle;

    constructor(player: Player) {
        this.player = player;
        this.obstacle = new Obstacle(this.player.name);
        this.setObstacle();
    }

    private setObstacle(): void {
        const obstaclePosition: number = this.player.position + this.player.getSpeed();
        this.obstacle.position = obstaclePosition > cellNumber ? cellNumber : obstaclePosition;
        this.obstacle.element.style.left = `${(cellLength * this.obstacle.position)}px`;
        this.obstacle.generate();
    }

    public round(): number {
        if (this.player.position !== 0) {
            this.setObstacle();
        }

        this.player.move();

        if (this.player.position > cellNumber) {
            this.player.position = cellNumber;
        }

        this.player.setSpeed(this.player.getSpeed() + this.obstacle.speedBoost);
        this.player.element.style.left = `${(cellLength * this.player.position)}px`;

        return this.player.position;
    }

    public reset(): void {
        this.player.position = 0;
        this.player.setSpeed();
        this.player.element.style.left = `${(cellLength * this.player.position)}px`;
        this.setObstacle();
    }
}

export default Track;