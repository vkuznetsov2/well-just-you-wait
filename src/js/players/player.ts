class Player
{
    public readonly name: string;
    private speed: number;
    private readonly initialSpeed: number;
    public position: number;
    public readonly element: any;

    constructor(name: string, speed: number, position: number = 0) {
        this.name = name;
        this.setSpeed(speed);
        this.initialSpeed = speed;
        this.position = position;
        [this.element] = [...document.getElementsByClassName(`player ${this.name}`)];
    }

    public getSpeed(): number {
        return Number(this.speed);
    }

    public setSpeed(speed = this.initialSpeed): void {
        speed < 1 ? this.speed = 1 : this.speed = speed;
        const [speedElement]: any = [...document.getElementsByClassName(`${this.name}-speed`)];
        speedElement.innerHTML = String(this.speed);
    }

    public move(): void {
        this.position = this.getSpeed() + this.position;
    }
}

export default Player;