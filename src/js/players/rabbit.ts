import Player from "./player";

class Rabbit extends Player
{
    constructor() {
        super('rabbit', 3);
    }
}

export default Rabbit;