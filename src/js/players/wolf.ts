import Player from "./player";

class Wolf extends Player
{
    constructor() {
        super('wolf', 2);
    }
}

export default Wolf;