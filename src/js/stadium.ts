import {cellNumber} from "./common";
import Track from "./track";

class Stadium {
    private readonly track1: Track;
    private readonly track2: Track;
    private readonly resultText: HTMLElement = document.getElementById('result');
    private readonly runButton: HTMLElement = document.getElementById('run');
    private readonly resetButton: HTMLElement = document.getElementById('reset');
    private readonly resultClasses: string[] = ['text-success', 'text-danger'];

    constructor(track1: Track, track2: Track) {
        this.track1 = track1;
        this.track2 = track2;
    }

    public run(): void {
        const rabbitPosition: number = this.track1.round();
        const wolfPosition: number = this.track2.round();

        if (wolfPosition >= rabbitPosition || rabbitPosition === cellNumber) {
            const resultClass: string = rabbitPosition === cellNumber ? this.resultClasses[0] : this.resultClasses[1];
            const resultText: string = rabbitPosition === cellNumber ? 'Н-У-,-П-О-Г-О-Д-И!!!' : 'П-О-Й-М-А-Л!!!';

            this.resultText.classList.add(resultClass);
            this.resultText.innerHTML = resultText;
            this.runButton.style.display = 'none';
            this.resetButton.style.display = 'inline';
        }
    }

    public reset(): void {
        this.track1.reset();
        this.track2.reset();

        this.resultText.classList.remove(...this.resultClasses);
        this.resultText.innerHTML = null;
        this.runButton.style.display = 'inline';
        this.resetButton.style.display = 'none';
    }
}

export default Stadium;