import {getRandomInt} from "./common";

class Obstacle
{
    private readonly playerName: string;
    public speedBoost: number = 0;
    public position: number = 0;
    public readonly element: any;

    constructor(playerName: string) {
        this.playerName = playerName;
        [this.element] = [...document.getElementsByClassName(`obstacle-${this.playerName}`)];
    }

    private getType(): string {
        return this.speedBoost > 0 ? 'berry' : 'stone';
    }

    public generate(): void {
        do {
            this.speedBoost = getRandomInt(-2, 2);
        } while (this.speedBoost === 0);

        this.element.className = `obstacle obstacle-${this.playerName} ${this.getType()}`;
    }
}

export default Obstacle;