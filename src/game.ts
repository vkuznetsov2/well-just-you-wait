import 'bootstrap';
import './scss/game.scss';
import './css/game.css';
import Track from './js/track';
import Rabbit from './js/players/rabbit';
import Wolf from './js/players/wolf';
import Stadium from './js/stadium';

const track1: Track = new Track(new Rabbit());
const track2: Track = new Track(new Wolf());
const stadium: Stadium = new Stadium(track1, track2);

document.getElementById('run').onclick = () => stadium.run();
document.getElementById('reset').onclick = () => stadium.reset();